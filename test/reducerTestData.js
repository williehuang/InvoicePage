const CHANGEITEM = 'invoice/changeitem';
const ADDITEM = 'invoice/additem';
const REMOVEITEM = 'invoice/removeitem';

const startItems = {
  items: [
    {
      itemName: 'Widget',
      quantity: 2,
      unitPrice: 10.00,
      totalPrice: 20.00,
    },
    {
      itemName: 'Cog',
      quantity: 2,
      unitPrice: 15.99,
      totalPrice: 31.98,
    },
    {
      itemName: 'Gear',
      quantity: 2,
      unitPrice: 109.99,
      totalPrice: 219.98,
    },
  ]
};

const widgetTotalPrice = 100;

const addNewBender = {
      itemName: 'Bender',
      quantity: 10,
      unitPrice: 59.99,
}

const newBenderTotalPrice = 599.90;

export { startItems, 
    CHANGEITEM, 
    ADDITEM, 
    REMOVEITEM, 
    widgetTotalPrice,
    addNewBender,
    newBenderTotalPrice
};