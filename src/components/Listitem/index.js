import React, { Component, PropTypes } from 'react';
import { TextField, Dropdown } from 'office-ui-fabric-react';
import _style from './Listitem.scss';

const INITIAL_STATE = {
    itemName: 'New Item',
    quantity: 0,
    unitPrice: 0,
    totalPrice: 0,
}

// I choose to use stateful component because submitting new field needs the state of fields 
// I want to keep the store containing only submitted values.

class Listitem extends Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
        this.isFieldValid = true;
        this.modifyField = this.changeField(props.newItem);
    }

    checkFieldValidity = (val, key) => {
        if (key === 'itemName') {
            return !(val === INITIAL_STATE.itemName);
        }
        if (key === 'quantity') {
            return val > 0;
        }
        return true;
    }

    provideUnitPriceForItem = (val, key) => {
        if (key === 'itemName') {
            const { itemReference } = this.props;
            const foundItem = itemReference.find((ele) => (ele.itemName === val));
            return foundItem ? foundItem.unitPrice : null;
        }
        return null;
    }

    /* this is a higher order function that would return either edit current field
        or editing a new empty field.
     */
    changeField = (isNewItem) => {
        const editFunction = isNewItem ? this.editNewField : this.editField;
        return (val, key) => {
            const unitPrice = this.provideUnitPriceForItem(val, key);
             editFunction(val, key, unitPrice);

        }
    }

    editField = (val, key, unitPrice) => {
        this.props.changeVal(this.props.index, key, val);
        if (typeof unitPrice == 'number' && !Number.isNaN(unitPrice)) {
            this.props.changeVal(this.props.index, 'unitPrice', unitPrice);
        }
    }

    editNewField = (val, key, unitPrice) => {
        if (!this.checkFieldValidity(val, key)) {
            this.isFieldValid = false;
            return;
        } else {
            this.isFieldValid = true;
        }
        if (typeof unitPrice == 'number' && !Number.isNaN(unitPrice)) {
            this.setState({
                itemName: val,
                unitPrice,
            });
        } else {
            this.setState({
                [key]: val,
            })
        }
    }

    validateAllFields = () => {
        const fields = this.state;
        for (let field in fields) {
            if (!this.checkFieldValidity(fields[field], field)) {
                return false;
            }
        }
        return true;
    }

    // if the new item are not entered correctly, an alert goes off.
    submit = (e) => {
        e.preventDefault();
        if (this.isFieldValid && this.validateAllFields()) {
            this.props.addVal(this.state);
            this.setState(INITIAL_STATE);
        } else {
            alert('can not add new item, one or more field is invalid');
        }
    }

    removeItem = (e) => {
        this.props.removeVal(this.props.index);
    }

    render() {
        const { newItem, index, itemReference } = this.props;
        const itemNameReference = itemReference.map(ele => (
                {
                    key: ele.itemName,
                    text: ele.itemName,
                }
            ));
        if (newItem) {
            itemNameReference.unshift({ 
                key: INITIAL_STATE.itemName,
                text: INITIAL_STATE.itemName,
            });
        }    
        const endOfLineIcon = newItem ? <i className="ms-Icon ms-Icon--AddTo" onClick={this.submit}></i> :
            <i className="ms-Icon ms-Icon--Cancel" onClick={this.removeItem}></i>;
        const { itemName, quantity, unitPrice, totalPrice } = newItem ? this.state : this.props;

    return (
        <div className={_style.itemcontainer}>
            <div className={_style.item}>
                <Dropdown
                    className="item-field"
                    selectedKey={itemName}
                    options={itemNameReference}
                    onChanged={(val)=> this.modifyField(val.key, 'itemName')}
                />
                <TextField
                    className="item-field"
                    value={quantity}
                    onChanged={(val) => this.modifyField(Number(val), 'quantity')}
                    validateOnFocusOut
                />
                <TextField
                    className="item-field"
                    value={Number(unitPrice).toFixed(2)}
                    disabled={true}
                />
                <TextField
                    className="item-field"
                    value={Number(totalPrice).toFixed(2)}
                    disabled={true}
                />
                {endOfLineIcon}
            </div>
        </div>
    );
    }
}

Listitem.propTypes = {
    itemName: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    unitPrice: PropTypes.number.isRequired,
    totalPrice: PropTypes.number.isRequired,
    changeVal: PropTypes.func.isRequired,
    addVal: PropTypes.func,
    itemReference: PropTypes.array.isRequired,
    removeVal: PropTypes.func,
    newItem: PropTypes.bool.isRequired,
    index: PropTypes.number.isRequired,
}

export default Listitem;
