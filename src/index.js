import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import store, { history } from './store';
import routes from './containers';
import fabriccss from 'office-ui-fabric-core/dist/css/fabric.min.css';
import _globalcss from './_globalCSS/_global.css'

const MOUNT = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <Router history={history} children={routes} />
  </Provider>, MOUNT);
  