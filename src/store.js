
import { browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from './redux/modules/reducer';

const browserMiddleware = routerMiddleware(browserHistory);
const middleware = applyMiddleware(
  logger(),
  browserMiddleware);

let preStore = compose(middleware,
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

const store = preStore(rootReducer);

export const history = syncHistoryWithStore(browserHistory, store);

if (module.hot) {
  module.hot.accept('./redux/modules/reducer', () => {
    store.replaceReducer(require('./redux/modules/reducer').default);
  });
}

export default store;
