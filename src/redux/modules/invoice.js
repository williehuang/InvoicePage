import { startItems } from '../../SampleData';
const CHANGEITEM = 'invoice/changeitem';
const ADDITEM = 'invoice/additem';
const REMOVEITEM = 'invoice/removeitem';

const initialState = startItems;

function modifyItems(items, { index, key, value }) {
  const newItems = [...items];
  if (Number.isInteger(index)) {
      const item = newItems[index];
      item[key] = value;
      item.totalPrice = item.quantity * item.unitPrice;
      newItems[index] = item;
  }
  return newItems;
}

function addNewItem(items, newItem = {}) {
  let [...allItems] = items;
  const foundDuplicateItem = allItems.find((ele) => {
    if (ele.itemName === newItem.itemName) {
      ele.quantity += newItem.quantity;
      ele.totalPrice += newItem.quantity * ele.unitPrice;
      return true;
    }
    return false;
  });
  if (foundDuplicateItem) {
    return allItems;
  } else {
    const { quantity, unitPrice } = newItem;
    const totalPrice = quantity * unitPrice;
    return [...allItems, Object.assign({}, newItem, {totalPrice})];
  } 
}

function removeItem(items, index) {
  return [...items.slice(0, index), ...items.slice(index + 1, items.length)]
}

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
      case CHANGEITEM:
        return {
          ...state,
          items: modifyItems(state.items, action.payload),
        };
      case ADDITEM:
        return {
          ...state,
          items: addNewItem(state.items, action.payload)
        };
      case REMOVEITEM:
        return {
          ...state,
          items: removeItem(state.items, action.payload)
        }; 
    default:
      return state;
  }
}

export function changeVal(index, key, value) {
  return {
    type: CHANGEITEM,
    payload: {
      index,
      key,
      value,
    }
  }  
}

export function addVal(item) {
  return {
    type: ADDITEM,
    payload: item,
  }
}

export function removeVal(index) {
  return {
    type: REMOVEITEM,
    payload: index,
  }
}