import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Summary from '../../components/Summary';

const taxPercent = 5; 
// if we want to make tax adjustable, I would save it separately in the store

function mapStateToProps(state) {
    const { invoice } = state;
    const calculatedSum = calculateSummary(invoice.items);
    return {
        subtotal: calculatedSum.subtotal,
        tax: calculatedSum.tax,
        total: calculatedSum.total,
        taxPercent,
    }
}

function calculateSummary(arr) {
    const [...items] = arr;
    const subtotal = Number(items.reduce((tot, ele) => (ele.totalPrice + tot), 0)).toFixed(2);
    const tax = Number(subtotal * (Number(taxPercent) / 100)).toFixed(2);
    const total = Number(Number(subtotal) + Number(tax)).toFixed(2);
    return {
        subtotal,
        total,
        tax,
    }
}

const PriceSummary = connect(mapStateToProps)(Summary);

export default PriceSummary;