import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import Invoice from './Invoice';

export default (
  <div>
    <Route path="/" component={App}>
      <IndexRoute component={Invoice} />
    </Route>
  </div>
);
