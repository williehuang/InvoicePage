import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import invoice from './invoice';

export default combineReducers({
    routing: routerReducer,
    invoice,
});
