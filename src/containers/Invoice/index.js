import React, { PropTypes } from 'react';
import { MessageBar } from 'office-ui-fabric-react';
import Listgroup from './Listgroup';
import PriceSummary from './PriceSummary';
import ListHeader from '../../components/ListHeader';
import _style from './Invoice.scss'

const InvoiceView = (props) => {
    return (
        <div className={_style.container}>
            <ListHeader />
            <Listgroup />
            <div className={_style.cardcontainer}>
                <MessageBar>
                    <PriceSummary />
                </MessageBar>
            </div>
        </div>
    );
};

export default InvoiceView;