import React, { PropTypes } from 'react';
import { Label } from 'office-ui-fabric-react';
import _style from './ListHeader.scss';

const ITEM_TITLE = 'Item';
const QTY_TITLE = 'Qty';
const PRICE_TITLE = 'Price';
const TOTAL_TITLE = 'Total';

const ListHeader = (props) => {
    return (
        <div className={_style.container}>
            <div className={_style.content}>
                <Label>{ITEM_TITLE}</Label>
                <Label>{QTY_TITLE}</Label>
                <Label>{PRICE_TITLE}</Label>
                <Label>{TOTAL_TITLE}</Label>
                <i className="ms-Icon ms-Icon--ChevronDown no-vis"></i>
            </div>
        </div>
    );
}

export default ListHeader;