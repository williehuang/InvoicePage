import React, { Component, PropTypes } from 'react';
import { TextField, Dropdown } from 'office-ui-fabric-react';
import _style from './Summary.scss';

const SUBTOTAL = 'Subtotal';
const DOLLARSIGN = '$';
const TAX = 'Tax ';
const TOTAL = 'Total';

const Summary = ({subtotal, tax, total, taxPercent}) => {
    return (
        <div className={_style.content}>
            <div className={_style.row}>
                <div>{SUBTOTAL}</div>
                <div>{DOLLARSIGN}{subtotal}</div>
            </div>
            <div className={_style.row}>
                <div>{`${TAX} (${taxPercent}%)`}</div>
                <div>{DOLLARSIGN}{tax}</div>
            </div>
            <div className={_style.row}>
                <div>{TOTAL}</div>
                <div>{DOLLARSIGN}{total}</div>
            </div>
        </div>
    );
}

Summary.propTypes = {
    subtotal: PropTypes.string.isRequired,
    tax: PropTypes.string.isRequired,
    total: PropTypes.string.isRequired,
}

export default Summary;