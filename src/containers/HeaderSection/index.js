import React, { PropTypes } from 'react';
import { Label } from 'office-ui-fabric-react';
import _style from './Header.scss';

const Title = 'Invoice';

const HeaderSection = (props) => {
    return (
        <div className={_style.container}>
            <div className="ms-fontSize-su">{Title}</div>
        </div>
    );
}

export default HeaderSection;