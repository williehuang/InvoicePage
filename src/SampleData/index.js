
const startItems = {
  items: [
    {
      itemName: 'Widget',
      quantity: 2,
      unitPrice: 10.00,
      totalPrice: 20.00,
    },
    {
      itemName: 'Cog',
      quantity: 2,
      unitPrice: 15.99,
      totalPrice: 31.98,
    },
  ]
};

const itemReference = [
    {
        itemName: 'Widget',
        unitPrice: 10.00,
    },
    {
        itemName: 'Cog',
        unitPrice: 15.99,
    },
    {
        itemName: 'Gear',
        unitPrice: 109.99,
    },
    {
        itemName: 'Marker',
        unitPrice: 8.99,
    },
    {
        itemName: 'Bender',
        unitPrice: 59.99,
    }
];

export { startItems, itemReference };