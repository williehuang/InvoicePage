
import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import classnames from 'classnames';
import _app from './App.scss';
import HeaderSection from '../HeaderSection';

function mapStateToProps(state) {
  return {
    
  };
}

const AppView = (props) => {
  return (
      <main className={_app.main}>
        <HeaderSection />
        <section>
          <div className={_app.content}>
            {props.children}
          </div>
        </section>
      </main>
    )
}

AppView.propTypes = {
  children: PropTypes.node.isRequired,
}

const App = connect(mapStateToProps, { pushState: push })(AppView);

export default App;
