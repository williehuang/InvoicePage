import test from 'ava';
import * as testData from '../../../test/reducerTestData';
import Reducer from './invoice';

test('edit an existing item quantity', t => {
    const newState = Reducer(testData.startItems, {
        type: testData.CHANGEITEM,
        payload: {
            index: 2,
            key: 'quantity',
            value: 10,
        },
    });
    t.is(newState.items[2].quantity, 10);
});

test('edit an existing item name', t=> {
    const newState = Reducer(testData.startItems, {
        type: testData.CHANGEITEM,
        payload: {
            index: 1,
            key: 'itemName',
            value: 'Marker',
        },
    });
    t.is(newState.items[1].itemName, 'Marker');
});

test('change quanity changes total price', t=> {
    const newState = Reducer(testData.startItems, {
        type: testData.CHANGEITEM,
        payload: {
            index: 0,
            key: 'quantity',
            value: '10',
        },
    });
    t.is(newState.items[0].totalPrice, testData.widgetTotalPrice);
});

test('add new item result in increase item array length', t=> {
    const newState = Reducer(testData.startItems, {
        type: testData.ADDITEM,
        payload: testData.addNewBender,
    });
    t.is(newState.items.length, 4);
});

test('add new item resulting in correct total price', t=> {
    const newState = Reducer(testData.startItems, {
        type: testData.ADDITEM,
        payload: testData.addNewBender,
    });
    t.is(newState.items[3].totalPrice, testData.newBenderTotalPrice);
});

test('removing an item decrease length', t => {
     const newState = Reducer(testData.startItems, {
        type: testData.REMOVEITEM,
        payload: 0,
    });
    t.is(newState.items.length, 2);
});

test('removing an item decrease length', t => {
     const newState = Reducer(testData.startItems, {
        type: testData.REMOVEITEM,
        payload: 0,
    });
    t.is(newState.items.length, 2);
});

test('removing an item result ensures array order', t => {
     const newState = Reducer(testData.startItems, {
        type: testData.REMOVEITEM,
        payload: 1,
    });
    t.is(newState.items[1].itemName, 'Gear');
});