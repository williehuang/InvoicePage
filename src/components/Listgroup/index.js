import React, { PropTypes } from 'react';
import { List, FocusZone, FocusZoneDirection } from 'office-ui-fabric-react';
import Listitem from '../Listitem';

const Listgroup = ({items, changeVal, addVal, removeVal, itemReference}) => {
    return (
        <div>
            <FocusZone direction={ FocusZoneDirection.vertical }>
            <List 
                items={items}
                onRenderCell={(item, index)=> (<Listitem
                    itemName={item.itemName}
                    quantity={item.quantity}
                    unitPrice={item.unitPrice}
                    totalPrice={item.totalPrice}
                    index={index}
                    newItem={false}
                    itemReference={itemReference}
                    changeVal={changeVal}
                    removeVal={removeVal}
                />)}
            />
            <Listitem 
                itemName=''
                quantity={0}
                unitPrice={0}
                totalPrice={0}
                newItem={true}
                index={0}
                changeVal={changeVal}
                itemReference={itemReference}
                addVal={addVal}
            />
            </FocusZone>
        </div>
    );
};

Listgroup.propTypes = {
    items: PropTypes.array.isRequired,
    changeVal: PropTypes.func.isRequired,
}

export default Listgroup;