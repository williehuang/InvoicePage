import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import ListGroup from '../../components/Listgroup';
import * as invoiceAction from '../../redux/modules/invoice';
import { itemReference } from '../../SampleData';

function mapStateToProps(state) {
    const { invoice } = state;
    return {
        items: invoice.items,
        itemReference,
    }
}

const Listgroup = connect(mapStateToProps, invoiceAction)(ListGroup);

export default Listgroup;