require('babel-polyfill');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const CONTENT_PATH = path.resolve('./public');
const ASSETS_PATH = path.resolve('./src/_assets/css');
const PRODUCTION = process.env.NODE_ENV === 'production';

module.exports = {
  devtool: 'eval-source-map',
  target: 'web', // Make web variables accessible to webpack, e.g. window
  entry: {
    app: './src/index.js',
    vendor: [
      'react',
      'react-dom',
      'react-redux',
      'react-router',
      'react-router-redux',
      'redux',
    ],
  },
  output: {
    path: CONTENT_PATH,
    //publicPath: 'http://localhost:3000/',
    filename: './dist/[name].bundle.js',
  },
  resolve: {
    modules: [
      ASSETS_PATH,
      'node_modules',
    ],
  },
  module: {
    preloaders: [
      {
        test: /\.jsx?$/, loader: 'eslint', exclude: /node_modules/,
      },
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react', 'stage-0'],
          cacheDirectory: true,
        },
      },
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader?sourceMap',
          'css?modules&importLoaders=1&localIdentName=[name]--[local]__[hash:base64:5]' +
          '!sass?sourceMap'),
        exclude: /node_modules/,
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: `url?limit=${PRODUCTION ? '100000' : '1000'}&name=resource/[hash:9].[ext]`,
      },
    ],
  },

  devServer: {
    contentBase: CONTENT_PATH,
  },
  plugins: [
    new ExtractTextPlugin('app.css', { allChunks: true }),

    new webpack.DefinePlugin({
      __PROD__: JSON.stringify(PRODUCTION),
      __ENV__: JSON.stringify(process.env.NODE_ENV),
      __TEST__: false,
    }),

    new webpack.optimize.UglifyJsPlugin({
      compress: PRODUCTION ? { warnings: false } : false,
    }),
  ],
};
