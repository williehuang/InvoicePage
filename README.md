# Invoice Page Demo
Invoice page that allows add, remove, update items and yields total cost.

## To run
1. `npm install`
2. `npm run dev`
3. `open browser and access localhost:3000`

## Running Reducer Tests
1. `npm test`

## Description
This is a invoice page that will display the initial data (from Sample Data folder) and the corresponding subtotal and total including tax.
1. If user wants to add a new item that already exists on the invoice the quanity will be accumulated and the total price will be recaculated accordingly.
2. If the user change the selection of item from the dropdown, the system will find the referenced unit price and display it accordingly.  The item name and unit price are mapped in sample data file. (Mocking data from rest API)
3. Deleting an item will kept the order.

## Architecture
### Redux Module
The redux module contains Action-Reducers.  The default export is the reducer and the exports are the actions. The default reducer will compose all the subsequent reducers such that the store will be composed in the same structure. I tried to keep the logics in the action as generic as possible such that data-flow is efficient and abstract.

### Page Routing
This is a SPA, thus, routing around pages can be completed using the pushState property.

### Containers
Each container would be connected to the store and action creators and pass those value/functions as properties to the view components.  Essentially, Redux specific logic operations should be occuring in these containers whereas view components should be only consuming properties that is being passed down.  This design ensures atomicity and modularization.

### Components
These are the view components for the application.  They are generally stateless functions.  However, depending on the circumstance, a react component can be used if certain internal state should be cached or managed without disrupting the store state.  ie. Having temporary/cached internal state to collect the required data before submitting the data as a group to store would be more efficient and individual submittions.

### Test
AVA testing framework is used for reducer testing.  Its a futuristic test runner that could test concurrently.  Each tests are saved along-side the files but with *.test.js

### Bundler
Webpack is used to bundle dependencies and source files together.  Configurations were set for ES6, React, Scss-pre-loaders etc.
